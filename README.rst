用Python进行医学数据可视化
=========================

这个项目的目的是通过实例证明可以只使用Python语言和开源的库来完成医学影像数据的三维可视化，包括面向3D打印的建模。

*感谢关注*

HowPy.cn 之 MedVisPy 小组